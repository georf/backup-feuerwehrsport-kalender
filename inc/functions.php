<?php

function date_str2js($date, $allday = 0) {
    $time = strtotime($date);
    
    if ($allday) $time -= 86400;

    // new Date(Jahr, Monat, Tag, Stunden, Minuten, Sekunden);
    return 'new Date('.
        date('Y', $time).','.
        intval(date('n', $time)-1).','.
        date('j', $time).','.
        date('G', $time).','.
        intval(date('i', $time)).','.
        intval(date('s', $time)).
        ')';
}
