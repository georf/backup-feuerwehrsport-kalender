<?php

// Funktion zum analysieren der HTTP-Auth-Header
function http_valid_login() {
    global $config;

    if (!isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) return false;
    if (empty($_SERVER['PHP_AUTH_USER'])) return false;
    if (empty($_SERVER['PHP_AUTH_PW'])) return false;

    foreach ($config['logins'] as $login) {
        if ($login['user'] == $_SERVER['PHP_AUTH_USER'] && $login['pass'] == $_SERVER['PHP_AUTH_PW']) {
            return true;
        }
    }
    return false;
}

function authorized() {
    global $config;

    if (http_valid_login()) {
        return true;
    }
}

function authorize() {
    global $config;

    header('WWW-Authenticate: Basic realm="'.$config['realm'].'"');
    header('HTTP/1.0 401 Unauthorized');

    echo '<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8">
                <title>401 - Unauthorized</title>
                <link rel="stylesheet" href="/auto-index-files/style.css" type="text/css">
            </head>
            <body>
                <h1>401 - Unauthorized</h1>
                <p><strong>Die angefragte Seite ist nicht verfügbar.</strong></p><p>Bitte nutzen Sie das Menü der Website oder einen anderen internen Link, um zu der gewünschten Seite zu navigieren.</p>		<p style="padding-left: 40px;"><a href="http://kalender.feuerwehrsport-teammv.de">kalender.feuerwehrsport-teammv.de</a></p>
                <div>
                    <p style="border-top: 1px solid #EEE;">MGVmedia Limbach &amp; Gaul GbR 2011 &ndash; <a href="http://www.mgvmedia.com/">http://www.mgvmedia.com</a></p>
                </div>
            </body>
        </html>
        ';
    exit;
}

