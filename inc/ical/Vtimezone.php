<?php

/*
 * This file is part of the eluceo/iCal package.
 *
 * (c) Markus Poerschke <markus@eluceo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


class Vtimezone extends Component
{
    public function __construct() {        
        $this->addComponent(new Daylight());
        $this->addComponent(new Standard());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return 'VTIMEZONE';
    }

    /**
     * {@inheritdoc}
     */
    public function buildPropertyBag()
    {
        /*
         * BEGIN:VTIMEZONE
TZID:Europe/Berlin
X-LIC-LOCATION:Europe/Berlin
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=3
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=10
END:STANDARD
END:VTIMEZONE*/
        $this->properties = new PropertyBag;
        $this->properties->set('TZID', 'Europe/Berlin');
        $this->properties->set('X-LIC-LOCATION', 'Europe/Berlin');
    }
}

abstract class MyTimezone extends Component {
    protected $osfrom;
    protected $osto;
    protected $tzname;
    protected $dtstart;
    protected $month;
    
    public function __construct($osfrom, $osto, $tzname, $dtstart, $month){
        $this->osfrom = $osfrom;
        $this->osto = $osto;
        $this->tzname = $tzname;
        $this->dtstart = $dtstart;
        $this->month = $month;
    }
    
    public function buildPropertyBag()
    {
        $this->properties = new PropertyBag;
        $this->properties->set('TZOFFSETFROM', $this->osfrom);
        $this->properties->set('TZOFFSETTO', $this->osto);
        $this->properties->set('TZNAME', $this->tzname);
        $this->properties->set('DTSTART', $this->dtstart);
        $this->properties->set('RRULE', 'FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH='.$this->month);
    }
        
}

class Daylight extends MyTimezone {
    public function __construct() {
        parent::__construct('+0100', '+0200', 'CEST', '19700329T020000', '3');
    }
    public function getType() {
        return 'DAYLIGHT';
    }
}

class Standard extends MyTimezone {
    public function __construct() {
        parent::__construct('+0200', '+0100', 'CST', '19701025T030000', '10');
    }
    public function getType() {
        return 'STANDARD';
    }
}
