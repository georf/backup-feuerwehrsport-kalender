<?php

include(__DIR__.'/inc/init.php');

if (isset($_GET['login']) && !authorized()) {
    authorize();
}

date_default_timezone_set('Europe/Berlin');

if (authorized()) {
    $birthdays = $db->getRows("
        SELECT `name`.`usd_value` AS `name`,
            `first`.`usd_value` AS `first`,
            `birthday`.`usd_value` AS `birthday`
        FROM `adm_users` `u`
        INNER JOIN `adm_user_data` `show` ON `u`.`usr_id` = `show`.`usd_usr_id`
        INNER JOIN `adm_user_data` `name` ON `u`.`usr_id` = `name`.`usd_usr_id`
        INNER JOIN `adm_user_data` `first` ON `u`.`usr_id` = `first`.`usd_usr_id`
        INNER JOIN `adm_user_data` `birthday` ON `u`.`usr_id` = `birthday`.`usd_usr_id`
        WHERE `show`.`usd_usf_id` = 26
        AND `show`.`usd_value` = '1'

        AND `name`.`usd_usf_id` = 1
        AND `first`.`usd_usf_id` = 2
        AND `birthday`.`usd_usf_id` = 10
    ");


    $trainings = $db->getRows("
        SELECT `dat_headline`,`dat_begin`,`dat_end`,`dat_all_day`,`dat_description`,`dat_headline`,`dat_location`
        FROM `adm_dates`
        WHERE `dat_cat_id` = 11
    ");

    $periodically = $db->getRows("
        SELECT `dat_headline`,`dat_begin`,`dat_end`,`dat_all_day`,`dat_description`,`dat_headline`,`dat_location`
        FROM `adm_dates`
        WHERE `dat_cat_id` = 14
    ");
} else {
    $periodically = array();
    $trainings = array();
    $birthdays = array();
}

$competitions = $db->getRows("
    SELECT `dat_headline`,`dat_begin`,`dat_end`,`dat_all_day`,`dat_description`,`dat_headline`,`dat_location`
    FROM `adm_dates`
    WHERE `dat_cat_id` = 10
");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Kalender - Team-MV</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link href='fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='jquery/jquery-1.9.1.min.js'></script>
<script src='jquery/jquery-ui-1.10.2.custom.min.js'></script>
<script src='fullcalendar/fullcalendar.min.js'></script>
<script src='qtip.js'></script>
<script>

	$(document).ready(function() {
    var tooltip = null;
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
                        firstDay: 1,
            timeFormat: 'H:mm',
			editable: false,
			events: [
<?php
$lines = array();
foreach ($birthdays as $b) {
    $t = strtotime($b['birthday']);
    $lines[] = '{'.
        ' title: \''.$b['first'].' '.$b['name'].'\', '.
        ' start: new Date('.date('Y').','.(date('n', $t)-1).','.date('j', $t).'),'.
        ' allDay: true,'.
        ' backgroundColor: \'#ADD8E6\','.
        ' borderColor: \'#95D4E9\''.
        '}';
    $lines[] = '{'.
        ' title: \''.$b['first'].' '.$b['name'].'\', '.
        ' start: new Date('.(date('Y')-1).','.(date('n', $t)-1).','.date('j', $t).'),'.
        ' allDay: true,'.
        ' backgroundColor: \'#ADD8E6\','.
        ' borderColor: \'#95D4E9\''.
        '}';
    $lines[] = '{'.
        ' title: \''.$b['first'].' '.$b['name'].'\', '.
        ' start: new Date('.(date('Y')+1).','.(date('n', $t)-1).','.date('j', $t).'),'.
        ' allDay: true,'.
        ' backgroundColor: \'#ADD8E6\','.
        ' borderColor: \'#95D4E9\''.
        '}';
}

foreach ($competitions as $c) {
    $lines[] = '{'.
        ' title: \''.$c['dat_headline'].'\', '.
        ' start: '.date_str2js($c['dat_begin']).','.
        ' end: '.date_str2js($c['dat_end'], $c['dat_all_day']).','.
        ' allDay: '.(($c['dat_all_day'] == '1')? 'true':'false').','.
        ' backgroundColor: \'#223344\','.
        ' description: \'<h4>'.$c['dat_location'].'</h4>'.preg_replace('^\r^', '', preg_replace('^\n^', '\n', str_replace("'", "\'", $c['dat_description']))).'\''.
        '}';
}

foreach ($trainings as $c) {
    $lines[] = '{'.
        ' title: \''.$c['dat_headline'].'\', '.
        ' start: '.date_str2js($c['dat_begin']).','.
        ' end: '.date_str2js($c['dat_end'], $c['dat_all_day']).','.
        ' allDay: '.(($c['dat_all_day'] == '1')? 'true':'false').','.
        ' backgroundColor: \'#FF0000\','.
        ' description: \'<h4>'.$c['dat_location'].'</h4>'.preg_replace('^\r^', '', preg_replace('^\n^', '\n', str_replace("'", "\'", $c['dat_description']))).'\''.
        '}';
}

foreach ($periodically as $c) {
    $lines[] = '{'.
        ' title: \''.$c['dat_headline'].'\', '.
        ' start: '.date_str2js($c['dat_begin']).','.
        ' end: '.date_str2js($c['dat_end'], $c['dat_all_day']).','.
        ' allDay: '.(($c['dat_all_day'] == '1')? 'true':'false').','.
        ' backgroundColor: \'#FFA500\','.
        ' description: \'<h4>'.$c['dat_location'].'</h4>'.preg_replace('^\r^', '', preg_replace('^\n^', '\n', str_replace("'", "\'", $c['dat_description']))).'\''.
        '}';
}
echo implode(",\n", $lines);
?>
			],

    eventAfterRender: function(event, element, view) {
        if (event.description) {
            var isIn = false;
            var $self = $(element);

            var show = function() {
                if (!tooltip) {
                    tooltip = $('<div class="tooltip"></div>').hide().appendTo('body');
                }

                var os = $self.offset();
                var left = os.left + $self.width()/2 - 150;
                var top = os.top + $self.height()+10;

                if (isIn) {
                    tooltip.css({
                        left: left,
                        top: top
                    });
                    tooltip.html(event.description).show();
                    console.log(tooltip);
                } else {
                    tooltip.hide();
                }
            };

            $self.mouseenter(function() {
                isIn = true;
                show();
            });
            $self.mouseleave(function() {
                isIn = false;
                show();
            });
        }
    }
		});

});


</script>
<style>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}

	#calendar {
		width: 900px;
		margin: 0 auto;
		}

.tooltip{
position:absolute;
width:300px;
padding:5px;
box-shadow:8px 8px 15px #666;
border:1px solid #ADD8E6;
z-index:1000;
background:#F3FDFF;
text-align:left;
}

</style>
</head>
<body>
<?php
if (!authorized()) {
    echo '<p><a href="?login">Login</a></p>';
}
?>
    <div id='calendar'></div>
    <?php
    $links = array();
    $links[] = '<a href="Wettkaempfe.ics">Wettkaempfe.ics</a>';
if (authorized()) {
    $links[] = '<a href="Geburtstage.ics">Geburtstage.ics</a>';
    $links[] = '<a href="Training.ics">Training.ics</a>';
    $links[] = '<a href="Regelmaessiges_Training.ics">Regelmaessiges_Training.ics</a>';
}

echo '<p>'.implode(' | ', $links).'</p>';
?>
</body>
</html>
