<?php

include(__DIR__.'/inc/init.php');

$types = array(
    'Wettkaempfe' => "
        SELECT
            `dat_headline` AS `title`,
            `dat_description` AS `description`,
            `dat_begin` AS `start`,
            `dat_end` AS `end`,
            `dat_all_day` AS `allday`,
            `dat_location` AS `location`
        FROM `adm_dates`
        WHERE `dat_cat_id` = 10
    ",
    'Geburtstage' => "
        SELECT
            CONCAT( `first`.`usd_value`, ' ', `name`.`usd_value`) AS `title`,
            NULL AS `description`,
            `birthday`.`usd_value` AS `start`,
            `birthday`.`usd_value` AS `end`,
            '1' AS `allday`,
            '' AS `location`
        FROM `adm_users` `u`
        INNER JOIN `adm_user_data` `show` ON `u`.`usr_id` = `show`.`usd_usr_id`
        INNER JOIN `adm_user_data` `name` ON `u`.`usr_id` = `name`.`usd_usr_id`
        INNER JOIN `adm_user_data` `first` ON `u`.`usr_id` = `first`.`usd_usr_id`
        INNER JOIN `adm_user_data` `birthday` ON `u`.`usr_id` = `birthday`.`usd_usr_id`
        WHERE `show`.`usd_usf_id` = 26
        AND `show`.`usd_value` = '1'

        AND `name`.`usd_usf_id` = 1
        AND `first`.`usd_usf_id` = 2
        AND `birthday`.`usd_usf_id` = 10
    ",
    'Training' => "
        SELECT
            `dat_headline` AS `title`,
            `dat_description` AS `description`,
            `dat_begin` AS `start`,
            `dat_end` AS `end`,
            `dat_all_day` AS `allday`,
            `dat_location` AS `location`
        FROM `adm_dates`
        WHERE `dat_cat_id` = 11
    ",
    'Regelmaessiges_Training' => "
        SELECT
            `dat_headline` AS `title`,
            `dat_description` AS `description`,
            `dat_begin` AS `start`,
            `dat_end` AS `end`,
            `dat_all_day` AS `allday`,
            `dat_location` AS `location`
        FROM `adm_dates`
        WHERE `dat_cat_id` = 14
    ",
);

if (!isset($_GET['type']) || !isset($types[$_GET['type']])) {
    $_GET['type'] = 'Wettkaempfe';
}

if ($_GET['type'] != 'Wettkaempfe' && !authorized()) {
    authorize();
}

$vCalendar = new Calendar($_GET['type'].'.kalender.feuerwehrsport-teammv.de');

$rows = $db->getRows($types[$_GET['type']]);

foreach ($rows as $row) {
    $vEvent = new Event();
    $vEvent->setDtStart(new DateTime($row['start']));
    $vEvent->setDtEnd(new DateTime($row['end']));
    $vEvent->setNoTime($row['allday']);
    $vEvent->setSummary($row['title']);
    $vEvent->setDescription(html_entity_decode(strip_tags($row['description'])));
    $vEvent->setUseTimezone('Europe/Copenhagen');

    if ($_GET['type'] == 'Geburtstage') {
        $vEvent->setFreqYearly();
    } else {
        $vEvent->setLocation($row['location']);
    }
    //$vEvent->setUrl('http://www.feuerwehr-warin.de/termine-'.$row['id'].'.html');

    /*if ($row['lat'] !== null && $row['lon'] !== null) {
        $vEvent->setGeo($row['lat'].';'.$row['lon']);
    }*/

    $vCalendar->addEvent($vEvent);
}

ob_end_clean();

header('Content-Type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename="'.$_GET['type'].'.ics"');
echo $vCalendar->render();


exit();
